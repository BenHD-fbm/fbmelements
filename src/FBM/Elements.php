<?php
namespace FBM;

class Elements
{
	public static $STYLE = array(
		'NULL' => '&mdash;',
		'DATE_FORMAT' => 'Y-m-d',
		'TIME_FORMAT' => 'h:i A',
	);

	/**
	 * @param \DateTime $datetime
	 * @param string $nullDisplay What to show when the $datetime is null. Defaults to Elements::$STYLE['NULL']
	 * @param string $dateFormat Defaults to Elements::$STYLE['DATE_FORMAT']
	 * @param string $timeFormat Defaults to Elements::$STYLE['TIME_FORMAT']
	 * @return string
	 */
	public static function DateTime($datetime, $nullDisplay = null, $dateFormat = null, $timeFormat = null)
	{
		if($nullDisplay == null) $nullDisplay = self::$STYLE['NULL'];
		if($dateFormat == null) $dateFormat = self::$STYLE['DATE_FORMAT'];
		if($timeFormat == null) $timeFormat = self::$STYLE['TIME_FORMAT'];
		
		if($datetime == null)
			return '<time class="null">' . $nullDisplay . '</time>';
		else
			return '<time><span class="date">' . $datetime->format($dateFormat) . '</span><span class="time">' . $datetime->format($timeFormat) . '</span></time>';
	}

	/**
	 * @param \DateTime $datetime
	 * @param string $nullDisplay What to show when the $datetime is null. Defaults to Elements::$STYLE['NULL']
	 * @param string $dateFormat Defaults to Elements::$STYLE['DATE_FORMAT']
	 * @return string
	 */
	public static function Date($datetime, $nullDisplay = null, $dateFormat = null)
	{
		if($nullDisplay == null) $nullDisplay = self::$STYLE['NULL'];
		if($dateFormat == null) $dateFormat = self::$STYLE['DATE_FORMAT'];
		
		if($datetime == null)
			return '<time class="null">' . $nullDisplay . '</time>';
		else
			return '<time><span class="date">' . $datetime->format($dateFormat) . '</span></time>';
	}

	/**
	 * @param \DateTime $datetime
	 * @param string $nullDisplay What to show when the $datetime is null. Defaults to Elements::$STYLE['NULL']
	 * @param string $timeFormat Defaults to Elements::$STYLE['TIME_FORMAT']
	 * @return string
	 */
	public static function Time($datetime, $nullDisplay = null, $timeFormat = null)
	{
		if($nullDisplay == null) $nullDisplay = self::$STYLE['NULL'];
		if($timeFormat == null) $timeFormat = self::$STYLE['TIME_FORMAT'];
		
		if($datetime == null)
			return '<time class="null">' . $nullDisplay . '</time>';
		else
			return '<time><span class="time">' . $datetime->format($timeFormat) . '</span></time>';
	}

	/**
	 * @param string $urlTemplate String containing "%s" which will be replaced with the target page number, ex: /search.php?term=stuff&page=%s&sort=things
	 * @param int $currentPage
	 * @param int $pageCount
	 * @return string HTML list of pagination links
	 */
	public static function Pagination($urlTemplate, $currentPage, $pageCount)
	{
		$pagination = '<ul class="pagination">';

		for($i = 0; $i < $pageCount; $i++)
		{
			if($i == $currentPage)
				$pagination .= '<span class="page-numbers current">' . ($currentPage + 1) . '</span>';
			else
			{
				// % in a URL can get encoded as %25, so we search for both:
				$pagination .= '<a class="page-numbers" href="' . preg_replace('/(%|%25)s/', $i, $urlTemplate) . '">' . ($i + 1) . '</a>';
			}
		}

		$pagination .= '</ul>';

		return $pagination;
	}

    /**
     * @param string[] $tabs An array of tabs, where "key" is the HREF for each tab link, and "value" is the human-readable title
     * @param string|null $activeKey The key in $tabs whose link should initially be marked "active"
     * @return string
     */
	public static function TabLinks($tabs, $activeKey = null)
    {
        $html = '<ul class="tab-links">';

        // defaults to first tab
        if($activeKey == null)
        {
            $tabKeys = array_keys($tabs);
            $activeKey = $tabKeys[0];
        }

        foreach($tabs as $href=>$title)
        {
            if($activeKey == $href)
                $html .= '<li class="active">';
            else
                $html .= '<li>';

            $html .= '<a href="' . $href . '">' . $title . '</a></li>';
        }

        $html .= '</ul>';

        return $html;
    }

    /**
     * @param float $value
     * @param string $content
     * @param bool $includeArrow
     * @return string
     */
    public static function GainLossWithIndicator($value, $content, $includeArrow = true)
    {
        $html = '<span class="' . ($value < 0 ? 'percent-gain-down' : 'percent-gain-up') . '">' . $content;

        if($includeArrow)
            $html .= self::IndicatorArrow($value);

        $html .= '</span>';

        return $html;
    }

    public static function IndicatorArrow($value)
    {
        return '<span class="' . ($value > 0 ? 'icon-indicator-up percent-gain-up' : 'icon-indicator-down percent-gain-down' ) . '"></span>';
    }
}
